import Component from '@ember/component';
import layout from './template';
import videojs from 'videojs';

export default Component.extend({
  layout,

  tagName: 'video',
  classNames: ['video-js','vjs-fluid'],
  attributeBindings: ['controls','preload','poster','src','autoplay','fluid'],

  src: null,
  controls: 'controls',
  preload: "auto",
  poster: null,
  autoplay: null,
  fluid: null,
  player: null,

  currPosition: 0,
  lastPosition: 0,
  maxPosition: 0,

  enableMaxPosition: false,

  updateprogress: function() {},

  didInsertElement() {

    var player = videojs(this.elementId, { aspectRatio: '16:9', startTime: this.get('lastPosition')});

    // if(this.get('src')) {
    //   player.src(this.get('src'));
    // }

    // player.on('loadedmetadata', evt => {
    //   player.currentTime(this.get('lastPosition'));
    // });

    player.on('seeking', evt => {
      // console.log('SEEKING C:',this.get('currPosition'),' L:',this.get('lastPosition'), ' M:', this.get('maxPosition'));
      if(this.get('enableMaxPosition') && this.get('maxPosition') < player.currentTime()) {
        this.set('currPosition', this.get('lastPosition'));
        player.currentTime(this.get('lastPosition'));
      }
    });

    player.on('timeupdate', evt => {
      // console.log('timeupdate C:',this.get('currPosition'),' L:',this.get('lastPosition'), ' M:', this.get('maxPosition'));
      if(this.get('maxPosition') < this.get('currPosition')) {
        this.set('maxPosition', this.get('currPosition'));
      }
      this.set('lastPosition', this.get('currPosition'));
      this.set('currPosition', player.currentTime());

      this.get('updateprogress')({
        position: this.get('lastPosition'),
        maxPosition: this.get('maxPosition')
      });
    });

    this.set('player',player)

  }

});
