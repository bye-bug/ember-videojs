/* eslint-env node */
'use strict';

var _environment;

var path = require('path');
var Funnel = require('broccoli-funnel');
var MergeTrees = require('broccoli-merge-trees');

module.exports = {
  name: require('./package').name,

  env: null,

  config(environment, appConfig) {
    _environment = environment;
    return {};
  },

  isDevelopingAddon() {
    return _environment == 'development';
  },

  included() {
    this._super.included.apply(this, arguments);
    this.import('vendor/video.js');
    this.import('vendor/shims/videojs.js');
  },

  treeForVendor(vendorTree) {
    var videojsTree = new Funnel(path.dirname(require.resolve('video.js')), {
      files: ['video.js'],
    });

    return new MergeTrees([vendorTree, videojsTree]);
  },

  treeForStyles: function (tree) {
    var styleTrees = [];
    var current = this;
    var app;

    do {
      app = current.app || app;
    } while (current.parent.parent && (current = current.parent));

    if (app.project.findAddonByName('ember-cli-sass')) {
      styleTrees.push(new Funnel(path.join('node_modules', 'video.js', 'src', 'css'), {
        destDir: 'ember-videojs'
      }));
    }

    if (tree) {
      styleTrees.push(tree);
    }

    return new MergeTrees(styleTrees, { overwrite: true });
  },

  afterInstall() {
    return this.addPackageToProject('video.js');
  }
};
